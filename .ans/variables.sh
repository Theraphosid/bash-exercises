#!/usr/bin/env bash
######################################################################## Created by: Theraphosid
### Date: 27.09.20
### Script is ment to practice and expend my skills is bash scripting### Version 1.0.1
#####################################################################

user="$1"

if [[ -z $user ]];then
	echo "Error you have not set a value"
elif [[ -n $user ]];then
	echo "Greetings $user Welcome to Bash Scripting"
fi
