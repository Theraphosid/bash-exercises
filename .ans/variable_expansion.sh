#!/usr/bin/env bash

##############################################
#Created by: Theraphosid
#Purpose of this script: require details from the user, print the details back for validation, check that first letter of ech detail is in capital letter and to save the data in a JSON format.
#Date: 17.3.21
##############################################	

echo "Please provide your details as follow."


read -p "Name:" name
for n in name;do
	if [[ "$name" =~ ^[a-z] ]];then
		echo "Please provide your name with an upper case letter"
		read -p "Name:" name
	fi
done
while [[ -z $name ]];do
	echo "Incorrect use of script please fill in all the details"
	exit 1
done


read -p "Last name:" lname
for l in lname;do
	if [[ "$lname" =~ ^[a-z] ]];then	
		echo "Please provide your last  name with an upper case letter"
		read -p "Last name:" lname
	fi
done
while [[ -z $lname ]];do
	echo "Incorrect use of script please fill in all the details"
	exit 1
done	


read -p "ID:" id
while [[ -z $id ]];do
	echo "Incorrect use of script please fill in all the details"
	exit 1
done


read -p "Address:" -a array
while [[ -z ${array[@]} ]];do
	echo "Incorrect use of script please fill in all the details"
	exit 1
done	
echo "Printing out your details for validation: $name, $lname, $id, ${array[@]}"


