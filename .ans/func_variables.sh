#!/usr/bin/env bash
################################################################################### Created by: Theraphosid
### Date: 27.09.20
### Script is ment to practice and expend my skills in bash scripting
### version 1.0.0
################################################################################

my_func(){
	local user="$1"
if [[ -z $user ]];then
	echo "Error you have not set a value"
elif [[ -n $user ]];then
	echo "Greetings $user Welcome to Bash Scripting"
fi
}
my_func "$1"
